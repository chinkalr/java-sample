FROM java:8

WORKDIR /opt

VOLUME /opt
EXPOSE 9092
MAINTAINER RISHI
COPY target/find-a-bank-1.0.0.jar /opt/find-a-bank-1.0.0.jar
CMD ["java","-jar","/opt/find-a-bank-1.0.0.jar"]
